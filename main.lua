require "components/map"
require "components/player"
require "components/camera"
require "components/shader"


function love.load()
    map = loadMap("assets/novomapa4")
    player = loadPlayer(2400, 2816, 500) 
    camera = loadCamera(player.act_x, player.act_y)
    shader = loadShader()
end

function love.update(dt)
    map:update(dt)
    player:update(dt)
    camera:move(player.act_x-375, player.act_y-250)
end

function love.draw()
	shader_draw()

	camera:set()

    map:draw()

    player:draw()

    camera:unset()

    shader_set()

    love.graphics.print('x: ' .. player.act_x, 5, 5)
    love.graphics.print('y: ' .. player.act_y, 5, 15)
    love.graphics.print('fps: ' .. tostring(love.timer.getFPS( )), 5, 25)
end