function loadCamera(x, y)
    local camera = {
      x = 0, 
      y = 0
    }

    function camera:set()
      love.graphics.push()
      love.graphics.translate(-self.x, -self.y)
    end

    function camera:unset()
      love.graphics.pop()
    end

    function camera:move(x, y)
      camera.x = x
      camera.y = y
    end

    function camera:update(dt)
    end

    return camera
end