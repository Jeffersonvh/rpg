function loadMap(path)
    local map = require(path)

    map.quads = {}
    map.objects = {}
    images = {}

    -- carregando os tileset's
    for i, tileset in ipairs(map.tilesets) do
        map.tileset = tileset

        images[i] = love.graphics.newImage(tileset.image)

        map.frame = 0
        map.timer = 0
        map.maxTimer = 0.1

        map.animatedTiles = {}

        for t, tile in ipairs(map.tileset.tiles) do
            map.animatedTiles[tile.id] = tile
        end

        for y = 0, (tileset.imageheight / tileset.tileheight) - 1 do
            for x = 0, (tileset.imagewidth / tileset.tilewidth) - 1 do
                local quad = love.graphics.newQuad(
                    x * tileset.tilewidth,
                    y * tileset.tileheight,
                    tileset.tilewidth,
                    tileset.tileheight,
                    tileset.imagewidth,
                    tileset.imageheight
                )
                table.insert(map.quads, quad)
            end
        end
    end

    -- Carregando os objetos
    for i, layer in ipairs(map.layers) do
        if (layer.type == "objectgroup") then
            for j, object in ipairs(layer.objects) do
                if object.type == "" then
                   table.insert(map.objects, {position = {x = object.x, y = object.y}, 
                                              size = {x = object.width, y = object.height}})
                end
            end
        end
    end

    function map:update(dt)
        if self.timer >= self.maxTimer then
            self.frame = self.frame + 1 
            self.timer = 0
        end
        self.timer = self.timer + dt 
    end

    function map:draw()
        for i, layer in ipairs(self.layers) do
            if layer.type == 'tilelayer' then
                drawLayer(layer)
            end
        end
    end

    function drawLayer(layer)
        for y = 0, layer.height - 1 do
            for x = 0, layer.width - 1 do
                for j, tileset in ipairs(map.tilesets) do
                    local index = (x + y * layer.width) + 1
                    local tid = layer.data[index]
                
                    if tid ~= 0 then
                        local quad = map.quads[tid]
                        local xx = x * map.tilewidth
                        local yy = y * map.tileheight
                        
                        if map.tilewidth ~= tileset.tilewidth then
                            xx = x * map.tilewidth
                        end

                        if map.tileheight ~= tileset.tileheight then
                            yy = y * map.tileheight-32
                        end

                        if layer.data[index] >= tileset.firstgid and layer.data[index] < (tileset.tilecount+tileset.firstgid) then
                            love.graphics.draw(
                                images[j],
                                quad,
                                xx,
                                yy
                            ) 
                        end  
                    end
                end
            end
        end
    end

    return map
end