function loadPlayer(ax, ay, speed)
    local player = {
        image = love.graphics.newImage("assets/images/fallen.jpg"),
        act_x = ax,
        act_y = ay,
        pla_w = 32,
        pla_h = 32,
        speed = speed
    }
 
    function player:update(dt)
        if love.keyboard.isDown('left') then
            if player:collisionCheck() then
                self.act_x = self.act_x - 32
            end
        elseif love.keyboard.isDown('right') then
            if player:collisionCheck() then
                self.act_x = self.act_x + 32
            end
        end
        if love.keyboard.isDown('up') then
            if player:collisionCheck() then
                self.act_y = self.act_y - 32
            end
        elseif love.keyboard.isDown('down') then
            if player:collisionCheck() then
                self.act_y = self.act_y + 32
            end
        end
    end
     
    function player:draw()
        --love.graphics.rectangle("fill", self.act_x, self.act_y, 32, 32)
        love.graphics.draw(player.image, self.act_x, self.act_y)
    end

    function player:collisionCheck()
        for j, object in ipairs(map.objects) do
            if player.act_x < object.position.x+object.size.x and 
               object.position.x < player.act_x+player.pla_w and 
               player.act_y < object.position.y+object.size.y and 
               object.position.y < player.act_y+player.pla_h then
                return false
            end
        end
        return true
    end

    return player
end