function loadShader( )
	local shader_code = [[
		struct Light {
		    vec2 position;
		    vec3 diffuse;
		    float power;
		};

		extern Light lights[1];
		extern int num_lights;

		extern vec2 screen;

		const float constant = 1.0;
		const float linear = 0.09;
		const float quadratic = 0.000000001;

		vec4 effect(vec4 color, Image image, vec2 uvs, vec2 screen_coords){
		    vec4 pixel = Texel(image, uvs);

		    vec2 norm_screen = screen_coords / screen;
		    vec3 diffuse = vec3(0);

		    for (int i = 0; i < num_lights; i++) {
		        Light light = lights[i];
		        vec2 norm_pos = light.position / screen;
		        
		        float distance = length(norm_pos - norm_screen) * light.power;
		        float attenuation = 1.0 / (linear * distance + constant + quadratic);
		        diffuse += light.diffuse * attenuation;
		    }

		    diffuse = clamp(diffuse, 0.0, 1.0);

		    return pixel * vec4(diffuse, 1.0);
		}

		]]

	local shader = love.graphics.newShader(shader_code)

	function shader_draw(power)
		love.graphics.setShader(shader)

		shader:send("screen", {
	        love.graphics.getWidth(),
	        love.graphics.getHeight()
	    })

		shader:send("num_lights", 1)

		do
	       local aura = "lights[" .. 0 .."]"
	       shader:send(aura .. ".position", {love.graphics.getWidth() / 2 - 16, love.graphics.getHeight() / 2 - 16})
	       shader:send(aura .. ".diffuse", {1, 1, 1})
	       shader:send(aura .. ".power", 100)
	    end
	end

	function shader_set()
		love.graphics.setShader()
	end

	return shader_code
end