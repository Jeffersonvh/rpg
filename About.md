# Folclore RPG

### Descrição
Um jogo que consiste em dar visibilidade ao folclore brasileiro, fazendo isso de modo divertido e dinâmico, mostrando um pouco da história dos personagens que fazem parte do folclore.

### Objetivos
Construir uma história dentro do jogo, onde o jogador vai poder tomar suas decisões e investigar misões atribuidas a ele, que para concluir será necessário explorar o mapa e enfrentar adversários. 
Ao final de sua jornada o jogador poderá compartilhar um resumo de escolhas durante as missões em formato de uma história. Para isso será criado um fórum aonde a comunidade pode ler e comentar na história de outros jogadores.

### Projetos futuros
Fazer do jogo mais interativo, construindo uma plataforma multiplayer e implementar missões mais complexas e difficiles para serem concluídas em grupo.
